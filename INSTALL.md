Digital Design and Computer Organization Lab Component Solution Manual for VTU.

[[_TOC_]]

## Installation
This lab requires two tools
- logisim-evolution
- verilog

We are using Ubuntu 22.04 for this lab. We next outline the installation procedure for the two tools used in the lab.



### Installing logisim-evolution  

![logisim-evolution-logo](Syllabus/logisim-evolution-logo.png)

logisim-evolution can be installed from the Snapstore link provided below  
https://snapcraft.io/logisim-evolution

It can be installed from the command line using the following command.  
**`$ sudo snap install logisim-evolution`**

### Installing verilog  

For Ubuntu 22.04 you can install verilog and gtkwave with the following command  

**`$ sudo apt install verilog gtkwave`**

### Verifying your verilog installation 

to verify your verilog installation and see version information eun the following command  

**`$ iverilog -V`**  

it should display the following  

**Icarus Verilog version 11.0 (stable) ()**  

If you are using older version of Ubuntu then you will have a older version of iverilog (probably 10.3) to install the latest version of iverilog do the folowing

Download the iverilog deb package from this link  
http://ftp.de.debian.org/debian/pool/main/i/iverilog/iverilog_11.0-1_amd64.deb  

Install it from the terminal using the following command  
**`$ sudo dpkg -i iverilog_11.0-1_amd64.deb`**   


