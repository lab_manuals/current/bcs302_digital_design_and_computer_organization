// Testbench for 2-to-1 Multiplexer

`timescale 1ns/1ps

module tb_mux2to1;

  reg a, b, sel;
  wire y_structural, y_dataflow, y_behavioral;

  // Instantiate structural, data flow, and behavioral models
  mux2to1_structural uut_structural (.a(a), .b(b), .sel(sel), .y(y_structural));
  mux2to1_dataflow uut_dataflow (.a(a), .b(b), .sel(sel), .y(y_dataflow));
  mux2to1_behavioral uut_behavioral (.a(a), .b(b), .sel(sel), .y(y_behavioral));

  // Stimulus
  initial begin
    a = 1; b = 0; sel = 0; #10;
    a = 0; b = 1; sel = 1; #10;
    a = 1; b = 1; sel = 0; #10;
    $finish;
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("mux2to1_tb.vcd");
    $dumpvars(0, tb_mux2to1);
  end

endmodule

