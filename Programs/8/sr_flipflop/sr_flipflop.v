module sr_flipflop (
  input wire s, // Set input
  input wire r, // Reset input
  input wire clk, // Clock input
  output reg q, // Q output
  output reg q_bar // Q' output
);

  always @(posedge clk or posedge s or posedge r) begin
    if (s) begin
      q <= 1'b1;
      q_bar <= 1'b0;
    end else if (r) begin
      q <= 1'b0;
      q_bar <= 1'b1;
    end else begin
      q <= q;
      q_bar <= q_bar;
    end
  end

endmodule
