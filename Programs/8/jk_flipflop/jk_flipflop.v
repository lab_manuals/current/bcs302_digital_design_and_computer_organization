module jk_flipflop (
  input wire j, // J input
  input wire k, // K input
  input wire clk, // Clock input
  output reg q, // Q output
  output reg q_bar // Q' output
);

  always @(posedge clk or posedge j or posedge k) begin
    if (j && ~k) begin
      q <= 1;
      q_bar <= 0;
    end else if (~j && k) begin
      q <= 0;
      q_bar <= 1;
    end else if (j && k) begin
      q <= ~q;
      q_bar <= q;
    end else begin
      q <= q;
      q_bar <= q_bar;
    end
  end

endmodule
