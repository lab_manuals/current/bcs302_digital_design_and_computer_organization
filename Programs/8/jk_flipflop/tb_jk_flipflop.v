`timescale 1ns/1ps

module tb_jk_flipflop;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg j, k, clk;
  wire q, q_bar;

  // Instantiate the JK Flip-Flop
  jk_flipflop flipflop_inst (.j(j), .k(k), .clk(clk), .q(q), .q_bar(q_bar));

  // Initial block for stimulus
  initial begin
    // Test case 1: Set the flip-flop (j=1, k=0)
    j = 1; k = 0; clk = 0;
    #10; // Wait for a few clock cycles

    // Test case 2: Clock transitions (clk=1)
    j = 0; k = 0; clk = 1;
    #10;

    // Test case 3: Reset the flip-flop (j=0, k=1)
    j = 0; k = 1; clk = 0;
    #10;

    // Test case 4: Clock transitions (clk=1)
    j = 0; k = 0; clk = 1;
    #10;

    // Test case 5: No Set or Reset (j=0, k=0)
    j = 0; k = 0; clk = 0;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge clk) begin
    $display("Time=%0t: j=%b, k=%b, clk=%b, q=%b, q_bar=%b", $time, j, k, clk, q, q_bar);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_jk_flipflop.vcd");
    $dumpvars(0, tb_jk_flipflop);
  end

endmodule
