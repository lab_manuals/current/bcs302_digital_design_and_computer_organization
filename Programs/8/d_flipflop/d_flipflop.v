module d_flipflop (
  input wire d, // D input
  input wire clk, // Clock input
  output reg q, // Q output
  output reg q_bar // Q' output
);

  always @(posedge clk) begin
    q <= d;
    q_bar <= ~d;
  end

endmodule
