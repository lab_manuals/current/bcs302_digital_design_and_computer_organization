`timescale 1ns/1ps

module tb_d_flipflop;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg d, clk;
  wire q, q_bar;

  // Instantiate the D Flip-Flop
  d_flipflop flipflop_inst (.d(d), .clk(clk), .q(q), .q_bar(q_bar));

  // Initial block for stimulus
  initial begin
    // Test case 1: Set the flip-flop (d=1)
    d = 1; clk = 0;
    #10; // Wait for a few clock cycles

    // Test case 2: Clock transitions (clk=1)
    d = 0; clk = 1;
    #10;

    // Test case 3: Reset the flip-flop (d=0)
    d = 0; clk = 0;
    #10;

    // Test case 4: Clock transitions (clk=1)
    d = 1; clk = 1;
    #10;

    // Test case 5: No Set or Reset (d=0)
    d = 0; clk = 0;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge clk) begin
    $display("Time=%0t: d=%b, clk=%b, q=%b, q_bar=%b", $time, d, clk, q, q_bar);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_d_flipflop.vcd");
    $dumpvars(0, tb_d_flipflop);
  end


endmodule
