## Question
**Given a 4-variable logic expression, simplify it using appropriate technique and simulate the same using basic gates.**

## Solution

Let's consider the following 4-variable logic expression:  
$F(A,B,C,D) = \sum(0,1,2,3,4,5,6,7,8,9,10,11,14)$  
  
This expression represents a sum of minterms for a 4-variable function. We can simplify it using a technique like Karnaugh Maps (K-maps) and then simulate the simplified expression using basic gates.  
### K-Map method
![K-Map](res/kmap.jpeg)  

Using the K-map method, we can simplify the expression:  

$F(A,B,C,D) = (\bar{B} + \bar{C} + A\bar{D})$

### Truth Table
![Truth Table](res/truthTable.png)  

### Open Logisim Evolution:


- Launch Logisim Evolution after installation.
### Create a New Project:

- Click on "File" in the menu bar.
- Choose "New" to create a new project.

### Add Gates:


- Drag and drop the required gates onto the canvas.
- Use the "Input" pin for input variables $(A, B, C, D)$.
- Use the "NOT" gate for negation $(\bar{B}, \bar{C}, A\bar{D})$.
- Use the "OR" gate for the OR operation.
- Use the "Output" pin for the output


### Connect Gates:

- Connect the gates appropriately to represent the logic expression.
- Connect the inputs (A, B, C, D) to the gates.
- Connect the output of each NOT gate to the OR gate.  

The final circuit looks as follows:  
![Circuit](res/circuit.png)  



### Simulate:

- Click on the "Simulate" toolbar button (green arrow) to start the simulation.
- To change input values change the mode by clicking on the Hand icon next to the Arrow Icon on the top
- Now you can change the input values by clicking on the input pins in the circuit.
- For example, set B = 0, C = 0, A = 1, D = 0.
- Observe the output of the circuit based on the given input values.

![Simulation Video](res/simulation.webm)  


### Verify Results:


- Confirm that the output matches the expected result for the given input values.

### Experiment:


- You can experiment with different input combinations to observe the behavior of the circuit.

### Save and Export:

- Save your project to keep your circuit design.
- The current Project file is made available as the file [Program01.circ](Program01.circ) in the repository.
- After downloading **Program01.circ** file, Open it using Logisim-evolution
- Optionally, you can export the circuit as an image or save it as a file.

