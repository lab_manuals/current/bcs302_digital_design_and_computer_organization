module full_subtractor (
  input a, b, bin,
  output diff, bout
);

  wire d1, b1, b2;

  // First XOR and AND gates for the difference and borrow-out
  half_subtractor hs1 (.a(a), .b(b), .diff(d1), .bout(b1));
  half_subtractor hs2 (.a(d1), .b(bin), .diff(diff), .bout(b2));

  // Final borrow-out is the OR of the intermediate borrows
  assign bout = b1 | b2;

endmodule
