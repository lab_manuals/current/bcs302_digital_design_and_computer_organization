`timescale 1ns/1ps

module tb_adders;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg a, b, cin;
  wire sum_ha, carry_ha, sum_fa, cout_fa;

  // Instantiate half adder and full adder
  half_adder ha_inst (.a(a), .b(b), .sum(sum_ha), .carry(carry_ha));
  full_adder fa_inst (.a(a), .b(b), .cin(cin), .sum(sum_fa), .cout(cout_fa));

  // Initial block for stimulus
  initial begin
    // Test case 1: a=1, b=0, cin=0
    a = 1; b = 0; cin = 0;
    #10;

    // Test case 2: a=0, b=1, cin=0
    a = 0; b = 1; cin = 0;
    #10;

    // Test case 3: a=1, b=1, cin=0
    a = 1; b = 1; cin = 0;
    #10;

    // Test case 4: a=1, b=1, cin=1
    a = 1; b = 1; cin = 1;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge carry_ha or posedge cout_fa) begin
    $display("Time=%0t: a=%b, b=%b, cin=%b, Sum_HA=%b, Carry_HA=%b, Sum_FA=%b, Cout_FA=%b",
             $time, a, b, cin, sum_ha, carry_ha, sum_fa, cout_fa);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_adders.vcd");
    $dumpvars(0, tb_adders);
  end

endmodule
