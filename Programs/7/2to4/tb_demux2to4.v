`timescale 1ns/1ps

module tb_demux2to4;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg [1:0] in;
  reg [1:0] sel;
  wire out0, out1, out2, out3;

  // Instantiate the 2-to-4 Demultiplexer
  demux2to4 demux_inst (.in(in), .sel(sel), .out0(out0), .out1(out1), .out2(out2), .out3(out3));

  // Initial block for stimulus
  initial begin
    // Test case 1: sel=2'b00, in=2'b01
    sel = 2'b00; in = 2'b01;
    #10;

    // Test case 2: sel=2'b01, in=2'b10
    sel = 2'b01; in = 2'b10;
    #10;

    // Test case 3: sel=2'b10, in=2'b00
    sel = 2'b10; in = 2'b00;
    #10;

    // Test case 4: sel=2'b11, in=2'b11
    sel = 2'b11; in = 2'b11;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sel or posedge in) begin
    $display("Time=%0t: sel=%b, in=%b, out0=%b, out1=%b, out2=%b, out3=%b", $time, sel, in, out0, out1, out2, out3);
  end

  initial begin
    $dumpfile("tb_demux2to4.vcd");
    $dumpvars(0, tb_demux2to4);
  end

endmodule
