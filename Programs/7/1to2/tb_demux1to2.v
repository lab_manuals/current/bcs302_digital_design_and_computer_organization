`timescale 1ns/1ps

module tb_demux1to2;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg in, sel;
  wire out0, out1;

  // Instantiate the 1-to-2 Demultiplexer
  demux1to2 demux_inst (.in(in), .sel(sel), .out0(out0), .out1(out1));

  // Initial block for stimulus
  initial begin
    // Test case 1: sel=0, in=1
    sel = 0; in = 1;
    #10;

    // Test case 2: sel=1, in=0
    sel = 1; in = 0;
    #10;

    // Test case 3: sel=0, in=0
    sel = 0; in = 0;
    #10;

    // Test case 4: sel=1, in=1
    sel = 1; in = 1;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sel or posedge in) begin
    $display("Time=%0t: sel=%b, in=%b, out0=%b, out1=%b", $time, sel, in, out0, out1);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_demux1to2.vcd");
    $dumpvars(0, tb_demux1to2);
  end


endmodule
