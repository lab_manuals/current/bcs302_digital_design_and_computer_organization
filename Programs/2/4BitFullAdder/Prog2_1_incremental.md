## Question
**Design a 4 bit full adder and simulate the same using basic gates.**


### Inputs  
- 4-bit number **A** $(A_3, A_2, A_1, A_0)$  
- 4-bit number **B** $(B_3, B_2, B_1, B_0)$  

### Output

- A 5-bit sum consisting of a **carry** bit $(C)$ and 4-bit **sum** $(S_3, S_2, S_1, S_0)$  

## Solution

We wiil see how to design a 4-bit full adder in a incremental manner in the following way:  

- We first design a 1-bit half-adder
- Next we design a 1-bit full-adder using the 2 1-bit half-adders  
- Next we design a 2-bit full-adder using the 2 1-bit full-adders  
- Finally we design a 4-bit full-adder using the 2 2-bit full-adders  


### Open Logisim Evolution:


- Launch Logisim Evolution after installation.
### Create a New Project:

- Click on "File" in the menu bar.
- Choose "New" to create a new project.

### 1-bit Half adder
- Drag and drop the required gates onto the canvas.
- Use the "Input" pin for input variables $A, B$.
- Use the "XOR" gate for the XOR operation.  
- Use the "AND" gate for the AND operation. 
- Use the "Output" pins for the outputs $S, C$

Connect the gates as shown in this diagram.  

![Half Adder](res/HA.png)

The truth table for half adder is as shown:  
![Truth Table](res/tru_HA.png)

Click on simulate and verify the output. The circuit file [Program02_1_0_half_adder.circ](Program02_1_0_half_adder.circ) is provided for your reference.  

### 1-bit Full adder  
Now that we have a 1-bit Half adder, we will now see how to obtain a full adder by using two half adders. Now place two half adders (you can use copy paste option) on the canvas and connect them as shown in this diagram.  
![Full Adder](res/FA.png)  

The truth table for full adder is as shown:  
![Truth Table](res/tru_FA.png)  

Click on simulate and verify the output. The circuit file [Program02_1_1_full_adder.circ](Program02_1_1_full_adder.circ) is provided for your reference.  

### 2-bit Full adder  
Now that we have a 1-bit Full adder, we will now see how to obtain a 2-bit full adder by using two 1-bit full adders. Now place two 1-bit Full adders (you can use copy paste option) on the canvas and connect them as shown in this diagram. Pass the carry of the first stage as input to second stage  

![2-bit Full Adder](res/2FA.png)  

The truth table for full adder is as shown:  
![Truth Table](res/tru_2FA.png)  

Click on simulate and verify the output. The circuit file [Program02_1_2_bit_full_adder.circ](Program02_1_2_bit_full_adder.circ) is provided for your reference.  

### 4-bit Full adder  
Now that we have a 2-bit Full adder, we will now see how to obtain a 4-bit full adder by using two 2-bit full adders. Now place two 2-bit Full adders (you can use copy paste option) on the canvas and connect them as shown in this diagram. Pass the carry of the first stage as input to second stage  

The final circuit looks as follows:  
![4-bit Full Adder](res/4FA.png)  


### Simulate:

- Click on the "Simulate" toolbar button (green arrow) to start the simulation.
- To change input values change the mode by clicking on the Hand icon next to the Arrow Icon on the top
- Now you can change the input values by clicking on the input pins in the circuit.
- Observe the output of the circuit based on the given input values.


### Verify Results:


- Confirm that the output matches the expected result for the given input values.

### Experiment:


- You can experiment with different input combinations to observe the behavior of the circuit.

### Save and Export:

- Save your project to keep your circuit design.
- The current Project file is made available as the file [Program02_1_4_bit_full_adder.circ](Program02_1_4_bit_full_adder.circ) in the repository.
- After downloading **Program02_1_4_bit_full_adder** file, Open it using Logisim-evolution
- Optionally, you can export the circuit as an image or save it as a file.
