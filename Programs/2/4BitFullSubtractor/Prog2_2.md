## Question
**Design a 4 bit full subtractor and simulate the same using basic gates.**


### Inputs  
- 4-bit number **A** $(A_3, A_2, A_1, A_0)$  
- 4-bit number **B** $(B_3, B_2, B_1, B_0)$ 
- 1-bit input borrow bit **$b_{in}$**   

### Output

- A 5-bit result consisting of a **borrow** bit **$b_{out}$** and 4-bit **difference** $D_3, D_2, D_1, D_0$  

## Solution

The 4 bit full subtractor can be represented by the following expressions:  
- $D_0 = A_0 \oplus B_0 \oplus b_0$
- $b_1 = (\overline{A_0 \oplus B_0} . b_0) + (\overline{A_0}) . B_0$  
- $D_1 = A_1 \oplus B_1 \oplus b_1$
- $b_2 = (\overline{A_1 \oplus B_1} . b_1) + (\overline{A_1}) . B_1$  
- $D_2 = A_2 \oplus B_2 \oplus b_2$
- $b_3 = (\overline{A_2 \oplus B_2} . b_2) + (\overline{A_2}) . B_2$  
- $D_3 = A_3 \oplus B_3 \oplus b_3$
- $b_4 = (\overline{A_3 \oplus B_3} . b_3) + (\overline{A_3}) . B_3$  


It's truth table is shown below partially  

![Truth Table](res/truthTable.png)

### Open Logisim Evolution:


- Launch Logisim Evolution after installation.
### Create a New Project:

- Click on "File" in the menu bar.
- Choose "New" to create a new project.

### Add Gates:


- Drag and drop the required gates onto the canvas.
- Use the "Input" pin for input variables $A_0, B_0, b_{in}, A_1, B_1, A_2, B_2, A_3, B_3$.
- Use the "XOR" gate for the XOR operation.  
- Use the "OR" gate for the OR operation.  
- Use the "NOT" gate for negation.  
- Use the "AND" gate for the AND operation.  
- Use the "Output" pins for the outputs $D_0, D_1, D_2, D_3, b_{out}$


### Connect Gates:

- Connect the gates appropriately to represent the logic expression.
- Connect the inputs to the gates.
- Connect the gates to the outputs.

The final circuit looks as follows:  
![Circuit](res/circuit.png)  


### Simulate:

- Click on the "Simulate" toolbar button (green arrow) to start the simulation.
- To change input values change the mode by clicking on the Hand icon next to the Arrow Icon on the top
- Now you can change the input values by clicking on the input pins in the circuit.
- Observe the output of the circuit based on the given input values.

![Simulation Video](res/simulation.webm)  


### Verify Results:


- Confirm that the output matches the expected result for the given input values.

### Experiment:


- You can experiment with different input combinations to observe the behavior of the circuit.

### Save and Export:

- Save your project to keep your circuit design.
- The current Project file is made available as the file [Program02_2.circ](Program02_2.circ) in the repository.
- After downloading **Program02_2.circ** file, Open it using Logisim-evolution
- Optionally, you can export the circuit as an image or save it as a file.
