`timescale 1ns/1ps

module tb_mux2to1;

  // Parameters
  parameter SIM_TIME = 50; // Simulation time

  // Signals
  reg a, b, sel;
  wire y;

  // Instantiate the 2:1 Multiplexer
  mux2to1 mux_inst (.a(a), .b(b), .sel(sel), .y(y));

  // Initial block for stimulus
  initial begin
    // Test case 1: sel=0, a=1, b=0
    sel = 0; a = 1; b = 0;
    #10;

    // Test case 2: sel=1, a=0, b=1
    sel = 1; a = 0; b = 1;
    #10;

    // Test case 3: sel=0, a=1, b=1
    sel = 0; a = 1; b = 1;
    #10;

    // Test case 4: sel=1, a=0, b=0
    sel = 1; a = 0; b = 0;
    #10;

    // End simulation
    $finish;
  end

  // Display results
  always @(posedge sel or posedge a or posedge b) begin
    $display("Time=%0t: sel=%b, a=%b, b=%b, y=%b", $time, sel, a, b, y);
  end

  // Dump variables to VCD file
  initial begin
    $dumpfile("tb_mux2to1.vcd");
    $dumpvars(0, tb_mux2to1);
  end


endmodule
