module mux8to1 (
  input wire [7:0] a,
  input wire [7:0] b,
  input wire [7:0] c,
  input wire [7:0] d,
  input wire [7:0] e,
  input wire [7:0] f,
  input wire [7:0] g,
  input wire [7:0] h,
  input wire [2:0] sel,
  output wire [7:0] y
);

  assign y = (sel == 3'b000) ? a :
            (sel == 3'b001) ? b :
            (sel == 3'b010) ? c :
            (sel == 3'b011) ? d :
            (sel == 3'b100) ? e :
            (sel == 3'b101) ? f :
            (sel == 3'b110) ? g :
            (sel == 3'b111) ? h : 8'b00000000;

endmodule
