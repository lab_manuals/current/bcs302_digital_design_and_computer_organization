module mux4to1 (
  input wire [3:0] a,
  input wire [3:0] b,
  input wire [3:0] c,
  input wire [3:0] d,
  input wire [1:0] sel,
  output wire [3:0] y
);

  assign y = (sel == 2'b00) ? a :
            (sel == 2'b01) ? b :
            (sel == 2'b10) ? c :
            (sel == 2'b11) ? d : 4'b0000;

endmodule
